#!/bin/bash
except() {
    find ./assets/minecraft/textures/colormap
    find ./assets/minecraft/textures/font
    find ./assets/minecraft/textures/gui
    find ./assets/minecraft/textures/particle
    find ./assets/minecraft/textures/entity/banner
    find ./assets/minecraft/textures/blocks/ -name "fire_layer_*.png"
    find ./assets/minecraft/textures/blocks/ -name "redstone_dust_*.png"
    find ./assets/minecraft/textures/models/armor
    find ./assets/minecraft/textures/entity/horse/armor
    echo ./assets/minecraft/textures/entity/alex.png
    echo ./assets/minecraft/textures/entity/steve.png
    echo ./assets/minecraft/textures/entity/banner_base.png
    #for i in `find ./assets -name '*.png.mcmeta'`; do
    #    #echo "$i"
    #    local fi="${i:0:${#i}-7}"
    #    [ -f "$fi" ] && echo "$fi"
    #    #find  2>/dev/null
    #done
}
remove() {
    find ./assets -name '*-.png'
    find . -name '*.old'
}
case "$1" in    
    "")
        OUT="../`basename "$PWD"`_redux"
        rm -Rf "$OUT"
        mkdir "$OUT"
        {
            echo "OUT: $OUT"
            for i in `find . -not -path "./.git/*" -and -not -path "./.git.old/*"`; do
                [ -d "$i" ] \
                    && mkdir -p "$OUT/$i" \
                    || mkdir -p "$OUT/`dirname "$i"`"
                # Try to convert
                convert -resize "50%" "$i" "$OUT/$i"\
                    && echo "    $i" \
                    || [ -d "$i" ] || {
                        cp "$i" "$OUT/$i" \
                        && echo "  + $i" \
                        || echo "! + $i"
                    }
            done
            for i in `except`; do
                [ -d "$i" ] || {
                    cp "$i" "$OUT/$i" \
                    && echo "  > $i" \
                    || echo "! > $i"
                }
            done
            for i in `remove`; do
                rm -Rf "$OUT/$i" \
                    && echo "  - $i" \
                    || echo "! - $i"
            done
            echo "OKAY"
        } 2>make_redux.log
        ;;
   "--rm"|"--removals")
        remove
        ;;
   "--except"|"--exceptions")
        except
        ;;
   *)
        echo "Invalid mode - $1"
esac
